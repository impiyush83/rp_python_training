def print_args(*args):
    print args
    for arg in args:
        print(arg)

def print_kwargs(**kwargs):
    print kwargs
    for k, v in kwargs.items():
        print("%s: %s" % (k, v))


my_list = ["one", "two", "three"]
print_args(*my_list)

my_dict = {"name": "Jane", "surname": "Doe"}
print_kwargs(**my_dict)
