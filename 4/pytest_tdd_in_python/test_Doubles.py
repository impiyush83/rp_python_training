from doubles import allow, expect


class User(object):

    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def get_last_city(self, city):
        pass


class SmsGateway(object):

    def __init__(self):
        pass

    def send_sms(self, user, message):
        print "PPPPPPPPPP"
        pass


class View(object):
    """
    Sample operations on User
    """
    def __init__(self, user, sms_gateway):
        self.user = user
        self.sms_gateway = sms_gateway

    def greet_user(self):
        name = self.user.get_name()
        return "Hello " + name

    def get_user_last_location(self, city1):
        return self.user.get_last_city(city1)

    # Comment this function and run tests
    def send_sms(self, message):
        # Comment following line and run tests to understand difference between allow & expect
        print "POOOddP"
        self.sms_gateway.send_sms(self.user, message)
        return None


class TestDoubles(object):
    def test_user_name(self):
        user = User("Sherlock")
        assert user.get_name() == "Sherlock"

    def test_greet_user(self):
        user = User("Sherlock")
        view = View(user, None)
        allow(user).get_last_city.and_return("Pune")
        assert view.get_user_last_location("Pune") == "Pune"

        allow(user).get_last_city.with_args('Pune').and_return("Pimpri")
        allow(user).get_last_city.with_args('Mumbai').and_return("Dadar")

        assert view.get_user_last_location("Pune") == "Pimpri"


    def test_sms_gateway(self):
        user = User("Sherlock")
        sms_gateway = SmsGateway()
        view = View(user, sms_gateway)

        expect(sms_gateway).send_sms(user, "JDJD")

        view.send_sms("JDJD")





